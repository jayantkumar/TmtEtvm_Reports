﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TmtEtvm_Reports.Models
{
    public class Reports
    {
        public List<DenominationWiseReport> DenominationReport { get; set; }
        public List<TotalCollectionWiseReport> TotalCollectionReport { get; set; }
        public List<StageWiseCollectionReport> StageWiseCollectionReport { get; set; }
        public List<StageWiseReport> StageWiseReports { get; set; }
        public List<TicketWiseReport> TicketWiseReports { get; set; }

    }
    public class Export //NOT IN USE
    {
        public void ToExcel(Reports reports, string fileneme)
        {
            // instantiate the GridView control from System.Web.UI.WebControls namespace
            // set the data source
            GridView gridview = new GridView();
            gridview.DataSource = reports.DenominationReport;
            gridview.DataBind();

            // Clear all the content from the current response
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.Buffer = true;
            // set the header
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename = " + fileneme + "_" + DateTime.Now + ".xls");
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Charset = "";
            // create HtmlTextWriter object with StringWriter
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                {
                    // render the GridView to the HtmlTextWriter
                    gridview.RenderControl(htw);
                    // Output the GridView content saved into StringWriter
                    HttpContext.Current.Response.Output.Write(sw.ToString());
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
            }
        }
    }






}