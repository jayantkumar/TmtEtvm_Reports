﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TmtEtvm_Reports.Models
{
    public class DenominationWiseReport
    {
        public string TotalFare { get; set; }
        public string Quantity { get; set; }
        public decimal TotalAmount { get; set; }
    }
}