﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TmtEtvm_Reports.Models
{
    public class ReportTypeMaster
    {
        public static string DenominationWiseReport = "1";
        public static string TotalCollectionWiseReport = "2";
        public static string StageWiseCollectionReport = "3";
        public static string StageWiseReport = "4";
        public static string TicketWiseReport = "5";

    }
}