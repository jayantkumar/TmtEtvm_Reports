﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TmtEtvm_Reports.Models
{
    public class StageWiseReport
    {
        public string Fare_Stage_number { get; set; }
        public string Source_F { get; set; }
        public string Source_H { get; set; }
        public string Source_L { get; set; }
        public string Source_P { get; set; }
        public string Destination_F { get; set; }
        public string Destination_H { get; set; }
        public string Destination_L { get; set; }
        public string Destination_P { get; set; }
    }
}