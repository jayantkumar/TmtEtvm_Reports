﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TmtEtvm_Reports.Models
{
    public class TicketWiseReport
    {
        public string KeyId { get; set; }
        public string F { get; set; }
        public string H { get; set; }
        public string L { get; set; }
        public string P { get; set; }
        public decimal TotalAmount { get; set; }
    }
}