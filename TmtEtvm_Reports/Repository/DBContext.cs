﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using TmtEtvm_Reports.Models;


namespace TmtEtvm_Reports.Repository
{
    public class DBContext
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter();
        public Reports GetReports(string FromDate,string ToDate,string WayBillNo,string ReportType)
        {
            Reports reports = new Reports();
            da = new SqlDataAdapter("exec etvmticketreport '" + FromDate + "' , '" + ToDate + "' , '" + WayBillNo + "' , '" + ReportType + "'", con);
            try
            {
                con.Open();
                da.Fill(ds);
                dt = ds.Tables[0];
                if(ReportType==ReportTypeMaster.DenominationWiseReport)
                {
                    reports.DenominationReport = (from DataRow dr in dt.Rows
                           select new DenominationWiseReport()
                           {
                               TotalFare = Convert.ToString(dr["TotalFare"]),
                               Quantity = Convert.ToString(dr["Quantity"]),
                               TotalAmount = Convert.ToDecimal(dr["TotalAmount"])

                           }).ToList();
                }
                else if (ReportType == ReportTypeMaster.TotalCollectionWiseReport)
                {
                    reports.TotalCollectionReport = (from DataRow dr in dt.Rows
                           select new TotalCollectionWiseReport()
                           {
                               PassengerType = Convert.ToString(dr["PassengerType"]),
                               Quantity = Convert.ToString(dr["Quantity"]),
                               TotalAmount = Convert.ToDecimal(dr["TotalAmount"])

                           }).ToList();
                }
                else if (ReportType == ReportTypeMaster.StageWiseCollectionReport)
                {
                    reports.StageWiseCollectionReport = (from DataRow dr in dt.Rows
                                select new StageWiseCollectionReport()
                                {
                                    Fare_Stage_number = Convert.ToString(dr["Fare_Stage_number"]),
                                    F = Convert.ToString(dr["F"]),
                                    H = Convert.ToString(dr["H"]),
                                    L = Convert.ToString(dr["L"]),
                                    P = Convert.ToString(dr["P"]),
                                    TotalAmount = Convert.ToDecimal(dr["TotalAmount"])


                                }).ToList();
                }
                else if (ReportType == ReportTypeMaster.StageWiseReport)
                {
                    reports.StageWiseReports = (from DataRow dr in dt.Rows
                           select new StageWiseReport()
                           {
                               Fare_Stage_number = Convert.ToString(dr["Fare_Stage_number"]),
                               Source_F= Convert.ToString(dr["Source_F"]),
                               Source_H= Convert.ToString(dr["Source_H"]),
                               Source_L= Convert.ToString(dr["Source_L"]),
                               Source_P= Convert.ToString(dr["Source_P"]),
                               Destination_F= Convert.ToString(dr["Destination_F"]),
                               Destination_H= Convert.ToString(dr["Destination_H"]),
                               Destination_L= Convert.ToString(dr["Destination_L"]),
                               Destination_P= Convert.ToString(dr["Destination_P"]),
                               

                           }).ToList();
                }
                else if (ReportType == ReportTypeMaster.TicketWiseReport)
                {
                    reports.TicketWiseReports = (from DataRow dr in dt.Rows
                           select new TicketWiseReport()
                           {
                               KeyId = Convert.ToString(dr["KeyId"]),
                               F = Convert.ToString(dr["F"]),
                               H = Convert.ToString(dr["H"]),
                               L= Convert.ToString(dr["L"]),
                               P= Convert.ToString(dr["P"]),
                               TotalAmount=Convert.ToDecimal(dr["TotalAmount"])

                           }).ToList();
                }
                else if (ReportType == "6")
                {
                   
                }
                
                con.Close();
            }
            catch(Exception ex)
            {
                con.Close();
            }
            
            return reports;
        }


    }
}