﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TmtEtvm_Reports.Models;
using TmtEtvm_Reports.Repository;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;

namespace TmtEtvm_Reports.Controllers
{
    public class HomeController : Controller
    {   
        Reports reports = new Reports();
        DBContext db = new DBContext();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        Export export = new Export();
        
        // GET: Home
        public ActionResult Index()
        {           
            return View();
        }
        [HttpGet]
        public ActionResult GetRecords(string txtFromDate, string txtToDate, string txtWawBillNo, string ddlSelectReportType)
        {
           
            if(ddlSelectReportType== ReportTypeMaster.DenominationWiseReport)
            {
                reports.DenominationReport = db.GetReports(txtFromDate, txtToDate, txtWawBillNo, ddlSelectReportType).DenominationReport;
                ViewBag.GrandTotal = reports.DenominationReport.Sum(x => x.TotalAmount);
                return PartialView("DenominationWise", reports.DenominationReport);
            }
            else if(ddlSelectReportType==ReportTypeMaster.TotalCollectionWiseReport)
            {
                reports.TotalCollectionReport = db.GetReports(txtFromDate, txtToDate, txtWawBillNo, ddlSelectReportType).TotalCollectionReport;
                ViewBag.GrandTotal = reports.TotalCollectionReport.Sum(x => x.TotalAmount);
                return PartialView("TotalCollectionWiseReport", reports.TotalCollectionReport);
            }
            else if (ddlSelectReportType == ReportTypeMaster.StageWiseCollectionReport)
            {
                reports.StageWiseCollectionReport = db.GetReports(txtFromDate, txtToDate, txtWawBillNo, ddlSelectReportType).StageWiseCollectionReport;
                return PartialView("StageWiseCollectionReport", reports.StageWiseCollectionReport);
            }
            else if (ddlSelectReportType == ReportTypeMaster.StageWiseReport)
            {
                reports.StageWiseReports = db.GetReports(txtFromDate, txtToDate, txtWawBillNo, ddlSelectReportType).StageWiseReports;
                return PartialView("StageWiseReport", reports.StageWiseReports);
            }
            else if (ddlSelectReportType == ReportTypeMaster.TicketWiseReport)
            {
                reports.TicketWiseReports = db.GetReports(txtFromDate, txtToDate, txtWawBillNo, ddlSelectReportType).TicketWiseReports;
                return PartialView("TicketWiseReport", reports.TicketWiseReports);
            }
            else if (ddlSelectReportType == "6")
            {
               
            }

            return PartialView("DenominationWise", reports.DenominationReport);
        }
        public ActionResult Excel()  //NOT IN USE
        {
            export.ToExcel(reports, "DenominationWise");
            return View();
        }
        

    }
}